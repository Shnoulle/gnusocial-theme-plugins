# Theme pour [GnuSocial](https://gnu.io/) : des idées

Ceci est un débit de projet sur ce qu'il me faudrait dans un nouveaux thème GnuSocial , avec ou sans plugin

## La base prioritaire

1. Full accessible (au plus possible)
    * RGAA
    * no-js
    * responsive
    * HTML5
2. Fonctionnement dynamique (ajax)
    * Mise à jour en direct par le haut (équivalent twitter/qvitter)
    * Mise à jour par le bas (existant)
    * Assistant de saisie utilisateur : complet (avec les domaines indiqué au besoin) : (équivalent twitter/qvitter)


## A la suite et idées 

1. Accès rapide à : notifications
2. 2 liens par utilisateur externe : local et final
3. Possibilité de répondre à une notice sur le réseau, même si elle n'est pas sur l'instance (lien direct vers)
    mastodon permet une recherche sur le réseau (par lien) : bon principe ?
4. A vérifier selon le protocole : [champs multi saisie](https://ostatus.shnoulle.net/notice/606618) (avec assitant js) pour les destinataires 
    Avec possibilité de l'ajouter sur les réponses à aussi
5. Fonctionnement déplié/plié cf https://mamot.fr/users/tetue/updates/43541


## Inspirations diverses

1. https://github.com/chimo/gs-prettySimple : malheureusement, pas responsive
2. https://github.com/chimo/gs-markdown
3. https://github.com/chimo/gs-emojis

## Décision à prendre

1. Framework css : il faut un fichier less , et ca serait cool de pouvoir le générer en direct (plus tard)
     1. bootstrap : lourd, quelquefois très imparfait. Les plus connu, très connu
     2. knacss : léger , pas besoin d'ajouter des classes sur certains éléments (input/textarea), mois connu
     3. plein d'autres
2. Theme complet (qvitter) ou theme base + plugin multiples compatible
     1. Theme détournant : peut être plus rapide/facile à construire (à confirmer), plus de rapidité à adapter pour le framework css
     2. Theme + plugin : ca reste compatible avec tous les futurs plugin et pourra être utilisé par d'autres